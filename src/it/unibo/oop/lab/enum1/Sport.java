/**
 * 
 */
package it.unibo.oop.lab.enum1;


public enum Sport {
	BASKET, SOCCER, TENNIS, BIKE, F1, MOTOGP, VOLLEY;
}
